class MwwondemandOrderNotification < ActiveRecord::Base
  belongs_to :mwwondemand_order
  has_many :mwwondemand_order_items, through: :mwwondemand_order
end
