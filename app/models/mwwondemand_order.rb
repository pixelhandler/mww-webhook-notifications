class MwwondemandOrder < ActiveRecord::Base
  has_many :mwwondemand_order_items
  has_one :mwwondemand_order_notification
end
