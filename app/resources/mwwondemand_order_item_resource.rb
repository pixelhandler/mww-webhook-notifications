class MwwondemandOrderItemResource < JSONAPI::Resource
  immutable

  attributes :state, :product_code, :line_number, :quantity, :created_at, :updated_at

  relationship :mwwondemand_order, to: :one
end
