class MwwondemandOrderNotificationResource < JSONAPI::Resource
  attributes :event, :title, :description, :timestamp, :created_at, :updated_at

  relationship :mwwondemand_order, to: :one
  relationship :mwwondemand_order_items, to: :many
end
