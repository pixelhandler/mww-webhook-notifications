class MwwondemandOrderResource < JSONAPI::Resource
  immutable

  attributes :vendor_po, :state, :created_at, :updated_at

  relationship :mwwondemand_order_notification, to: :one
  relationship :mwwondemand_order_items, to: :many
end
