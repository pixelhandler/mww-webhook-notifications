class ApplicationController < ActionController::Base
  include JSONAPI::ActsAsResourceController

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # Allow regular JSON type too
  def ensure_correct_media_type
    unless request.content_type == JSONAPI::MEDIA_TYPE || request.content_type == 'application/json'
      fail JSONAPI::Exceptions::UnsupportedMediaTypeError.new(request.content_type)
    end
  rescue => e
    handle_exceptions(e)
  end
end
