class MwwondemandOrderNotificationsController < ApplicationController

  def create
    mww_order = create_mww_order
    create_mww_order_items mww_order
    create_mww_notice(mww_order)
    render :status => :no_content, :nothing => true
  rescue Exception => e
    logger.error e
    render :status => :bad_request, :nothing => true
  end

  private

  def create_mww_order
    order_id = params[:data][:relationships]['mwwondemand-order']['data']['id']
    mww_order = params[:included]['mwwondemand-orders'].select {|o| o['id'] = order_id }
    order = MwwondemandOrder.new
    mww_order.first['attributes'].each {|k, v| order.assign_attributes k.underscore.to_sym => v }
    order.save
    order
  end

  def create_mww_order_items(mww_order)
    params[:included]['mwwondemand-order-items'].each do |item|
      order_item = MwwondemandOrderItem.new
      item['attributes'].each {|k, v| order_item.assign_attributes k.underscore.to_sym => v }
      order_item.mwwondemand_order = mww_order
      order_item.save
    end
  end

  def create_mww_notice(mww_order)
    notice = MwwondemandOrderNotification.new
    params[:data][:attributes].each {|k, v| notice.assign_attributes k.underscore.to_sym => v }
    notice.mwwondemand_order = mww_order
    notice.save
  end

end
