class CreateMwwondemandOrderItems < ActiveRecord::Migration
  def change
    create_table :mwwondemand_order_items do |t|
      t.string :state
      t.string :product_code
      t.integer :line_number
      t.integer :quantity

      t.timestamps null: false

      t.references :mwwondemand_order
    end
  end
end
