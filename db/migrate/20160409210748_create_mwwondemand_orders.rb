class CreateMwwondemandOrders < ActiveRecord::Migration
  def change
    create_table :mwwondemand_orders do |t|
      t.string :vendor_po
      t.string :state

      t.timestamps null: false
    end
  end
end
