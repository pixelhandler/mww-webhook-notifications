class CreateMwwondemandOrderNotifications < ActiveRecord::Migration
  def change
    create_table :mwwondemand_order_notifications do |t|
      t.string :event
      t.string :title
      t.string :description
      t.timestamp :timestamp

      t.timestamps null: false

      t.references :mwwondemand_order
    end
  end
end
