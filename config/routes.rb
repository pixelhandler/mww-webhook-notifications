Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  jsonapi_resources :mwwondemand_order_notifications
  jsonapi_resources :mwwondemand_orders
  jsonapi_resources :mwwondemand_order_items

end
